import 'package:flutter_driver/flutter_driver.dart';
import 'package:ogurets/ogurets.dart';
import 'package:test/test.dart';
import 'package:flutter_driver_fast_restart/flutter_driver_extensions.dart';

import 'steps/steps.dart';

// Usage: dart --enable-asserts test_driver/feature_tests.dart
void main() async {
  final timeout = Timeout(Duration(minutes: 30));

  var ogurets = new OguretsOpts()
    ..features(["test_driver/features"])
    ..failOnMissingSteps(true)
    ..step(Steps)
    ..hooks(RestartHook)
    ..useAsserts(false);

  FlutterDriver driver;

  group("Feature tests", () {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      ogurets.instance(FlutterWorld(driver));
    });

    test("all", () async {
      // Wrap into test so we can use `expect` in steps.
      final result = await ogurets.run();
      if (result.failed) throw "Feature tests failed";
    }, timeout: timeout);

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });
  }, timeout: timeout);
}

class FlutterWorld {
  final FlutterDriver driver;

  FlutterWorld(this.driver);
}

class RestartHook {
  final FlutterWorld _world;

  RestartHook(this._world);

  @Before()
  Future<void> restartBeforeScenario() async {
    await _world.driver.restart();
    await _world.driver.waitUntilNoTransientCallbacks();
  }
}
