import 'package:flutter_driver_fast_restart/flutter_driver_fast_restart.dart';

import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:gherkin_test/main.dart';

void main() {
  // ignore: close_sinks
  final restartController = RestartController();
  enableFlutterDriverExtension(
    silenceErrors: true, // std doesn't work on flutter web
    handler: (request) async {
      restartController.add(request);
      return "";
    },
  );
  timeDilation = 0.01; // Will speed up animations
  runApp(RestartWidget(
    restartController: restartController,
    builder: (_, data) => MyApp(initialRoute: data),
  ));
}
