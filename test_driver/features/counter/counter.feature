Feature: Counter
  The counter should be changed when the button is pressed.

  Background:
    Given I am on the counter screen

  Scenario Outline: Counter decreases when the button is pressed
    Given I expect the "counter" to be "0"
    When I tap the "decrement" button <times> times
    Then I expect the "counter" to be <result>

    Examples:
      | times | result |
      | 1     | "-1"   |
      | 10    | "-10"  |

  Scenario Outline: Counter increases when the button is pressed
    Given I expect the "counter" to be "0"
    When I tap the "increment" button <times> times
    Then I expect the "counter" to be <result>

    Examples:
      | times | result |
      | 1     | "1"    |
      | 10    | "10"   |
