Feature: Opening second screen
  The counter should be reflected in opened second screen.

  Background:
    Given I am on the counter screen

  Scenario: Result screen opens
    When I tap the "increment" button 2 times
    And I tap the "open" button
    Then I expect to see "Counter: 2" text
    