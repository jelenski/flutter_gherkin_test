Feature: Opening third screen
  Second screen should open third screen.

  Background:
    Given I am on the second screen with counter 7

  Scenario: Third screen opens after button press
    Given I expect to see "Counter: 7" text
    When I tap the "open" button
    And I expect to see "Hello world!" text
    