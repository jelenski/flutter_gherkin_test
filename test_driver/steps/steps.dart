import 'package:ogurets/ogurets.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_driver_fast_restart/flutter_driver_extensions.dart';
import 'package:test/test.dart';
import '../app_test.dart';
import '../_config.dart';

class Steps {
  final FlutterWorld _world;

  Steps(this._world);

  @StepDef(r'I am on the counter screen')
  void iAmOnTheCounterScreen() async {
    await _world.driver.restart("/");
    await _world.driver.waitUntilNoTransientCallbacks();
  }

  @StepDef(r'I am on the second screen with counter {int}')
  void iAmOnTheSecondScreen(int counter) async {
    await _world.driver.restart("/second/$counter");
    await _world.driver
        .waitFor(find.text("Second screen"))
        .timeout(timeoutDuration);
  }

  @StepDef(r'I expect to see {string} text')
  void iExpectToSeeText(String text) async {
    await _world.driver.waitFor(find.text(text)).timeout(timeoutDuration);
  }

  @StepDef(r'I expect the {string} to be {string}')
  void iExpectComponentToBe(String key, String text) async {
    final finder = find.byValueKey(key);
    await _world.driver.waitFor(finder).timeout(timeoutDuration);
    final actualText =
        await _world.driver.getText(finder).timeout(timeoutDuration);
    expect(actualText, text);
  }

  @StepDef(r'I tap the {string} (button|field)')
  void iTapTheButton(String key, String component) async {
    await _world.driver.tap(find.byValueKey(key)).timeout(timeoutDuration);
  }

  @StepDef(r'I tap the {string} button {int} times')
  void iTapTheButtonNTimes(String button, int times) async {
    final finder = find.byValueKey(button);
    for (var i = 0; i < times; i += 1) {
      await _world.driver.tap(finder).timeout(timeoutDuration);
    }
  }

  @StepDef(r'I type {string} text')
  void iTypeText(String text) async {
    await _world.driver.enterText(text);
  }

  @StepDef(r'I wait {int} second[s]{0,1}')
  void iWaitSecondss(int seconds) async {
    await Future.delayed(Duration(seconds: seconds));
  }
}
