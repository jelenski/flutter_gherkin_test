import 'package:flutter/material.dart';
import 'package:regex_router/regex_router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final String initialRoute;

  const MyApp({Key key, this.initialRoute = ""}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final router = RegexRouter.create({
      "/": (context, _) => MyHomePage(title: 'Flutter Demo Home Page'),
      "/second/:counter": (context, args) => SecondScreen(
            counter: args["counter"],
          )
    });

    return MaterialApp(
      title: 'Flutter Demo',
      initialRoute: initialRoute,
      onGenerateRoute: router.generateRoute,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  TextEditingController textController = TextEditingController();

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: textController,
              key: Key("input"),
            ),
            MaterialButton(
              key: Key("submit"),
              child: Text("Submit"),
              onPressed: () async {
                await showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      content: Text("Typed text: ${textController.text}"),
                    );
                  },
                );
              },
            ),
            Text(
              'You have pushed the button this many times:',
            ),
            Text('$_counter',
                style: Theme.of(context).textTheme.headline1,
                key: Key("counter")),
            MaterialButton(
              child: Text("Open second screen"),
              key: Key("open"),
              onPressed: () =>
                  Navigator.of(context).pushNamed("/second/$_counter"),
            ),
            MaterialButton(
              child: Text("Increment"),
              key: Key("increment"),
              onPressed: _incrementCounter,
            ),
            MaterialButton(
              child: Text("Decrement"),
              key: Key("decrement"),
              onPressed: _decrementCounter,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }
}

class SecondScreen extends StatelessWidget {
  final String counter;

  const SecondScreen({Key key, this.counter}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Second screen")),
      body: Column(
        children: <Widget>[
          Text("Counter: $counter"),
          MaterialButton(
            child: Text("Open third screen"),
            key: Key("open"),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(builder: (context) => ThirdScreen()),
            ),
          ),
        ],
      ),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Third screen")),
      body: Text("Hello world!"),
    );
  }
}
